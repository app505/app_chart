
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const canvasRenderService = new ChartJSNodeCanvas({ width: 500, height: 500, backgroundColour: 'rgba(255,255,255,1) ' });
const fs = require('fs');


const generateConfiguration = () => {
    return configuration = {
        type: 'line',
        data: { datasets: [] },
        options: {},

    }
}

const addConfiguration = (configuration, element) => {
    configuration?.data.datasets.push({ "data": element?.coords, "borderColor": element?.borderColor, "label": element?.description, "backgroundColor": element?.borderColor , "tension":element?.tension})
    return configuration;
}

const generateChartPNG = (configuration) => {
    (async () => {
        const imageBuffer = await canvasRenderService.renderToBuffer(configuration);
        fs.writeFileSync('/mnt/d/Node/1.png', imageBuffer);
    })();

}

module.exports = {
    generateConfiguration,
    addConfiguration,
    generateChartPNG
}