
const yargs = require('yargs');
const functions = require('./functions');

yargs.command({
    command: 'gen',  
    builder: {
        data: {
            type: 'string',
            demandOption: false,
        },
        options: {
            type: 'string',
            demandOption: false,
        }
    },
    handler(args) {      
      try {
       var configuration = functions.generateConfiguration()

        const parser =JSON.parse(args.data);
        
        parser.forEach(element => {
          configuration=   functions.addConfiguration(configuration, element)
          
        });

        functions.generateChartPNG(configuration)

} catch (error) {
        console.log('Error' )
      }
    }
}).parse(); 
