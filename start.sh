#!/bin/bash

node app gen --data='
[
    {
        "coords" : [
            {"x":"1", "y":"1"},
            {"x":"2", "y":"2"},
            {"x":"3", "y":"3"},
            {"x":"4", "y":"4"}
        ],
        "borderColor" : "rgb(1, 1, 255)" , 
        "description":"Результат",
        "tension":"0.3"
    } ,


    {
        "coords" : [
           {"x":"1", "y":"1"},
            {"x":"2", "y":"2.5"},
            {"x":"3", "y":"1"},
            {"x":"4", "y":"4"}
        ] , 
        "borderColor" : "rgb(255, 1, 1)", 
        "description":"Эталон",
        "tension":"0.3"
    }
    
]'

# code -r /mnt/d/Node/1.png
